from django.contrib import admin
from .models import TextPost, Comment
# Register your models here.

admin.site.register(TextPost)
admin.site.register(Comment)


