from django.forms import ModelForm
from posts.models import TextPost, Comment
from django import forms


class CreatePostForm(ModelForm):
    media_content = forms.FileField(required=False, widget=forms.FileInput(attrs={'accept':'image/*,video/*'}))
    class Meta:
        model = TextPost
        fields = ['title', 'content', 'media_content']

class CreateCommmentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ['body']
