# Generated by Django 2.2 on 2021-10-02 14:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0014_auto_20211001_0628'),
    ]

    operations = [
        migrations.AlterField(
            model_name='textpost',
            name='media_content',
            field=models.FileField(blank=True, null=True, upload_to='files/'),
        ),
    ]
