from os import environ
from django.db import models
from django.contrib.auth.models import User
# from users.models import Profile
from django.utils import timezone
from django.utils.text import slugify
from autoslug import AutoSlugField

from subreddit.models import Subreddit
from mimetypes import guess_type


# Create your models here.

# Share text posts and link posts
# Just the name. includes all types of posts
class TextPost(models.Model):
    title = models.TextField(null = False,
        blank = False,
        max_length=50
    )
    content = models.TextField(null=True, blank=True)
    date_posted = models.DateTimeField(default=timezone.now)
    author_id = models.ForeignKey(User, on_delete=models.CASCADE)
    subreddit_id = models.ForeignKey(Subreddit, on_delete=models.CASCADE)
    media_content = models.FileField(upload_to='files/', blank=True, null=True)
    likes = models.ManyToManyField(User, blank=True, related_name='likes')
    dislikes = models.ManyToManyField(User, blank=True, related_name='dislikes')

    def __str__(self):
        return self.title
    
    def get_absolute_url(self):
        return f"/r/{self.subreddit_id.slug}/{self.id}/"
    
    # to get comment with parent is none and active is true, we can use this in template
    def get_comments(self):
        return self.comments.filter(parent=None).filter(active=True)
        
    def media_type(self):
        """
        guess_type returns a tuple like (type, encoding) and we want to access
        the type of media file in first index of tuple
        """
        print(self.media_content.url)
        short = ""
        for ele in self.media_content.url:
            if ele =="?":
                break
            else:
                short+=ele

        type_tuple = guess_type(short, strict=True)
        print(type_tuple)
        try:
            if ("image" in type_tuple[0]):
                return "image"
            elif ("video" in type_tuple[0]):
                return "video"
            else:
                return ""
        except:
            return ""

class Comment(models.Model):
    post = models.ForeignKey(TextPost, on_delete=models.CASCADE, related_name="comments")
    author_id = models.ForeignKey(User, on_delete=models.CASCADE)
    subreddit = models.ForeignKey(Subreddit, on_delete=models.CASCADE)
    body = models.TextField()
    date_added = models.DateTimeField(default=timezone.now)
    parent = models.ForeignKey("self", null=True, related_name="children", blank=True, on_delete=models.CASCADE)
    active = models.BooleanField(default=True)
    likes = models.ManyToManyField(User, blank=True, related_name='comment_likes')
    dislikes = models.ManyToManyField(User, blank=True, related_name='comment_dislikes')

    class Meta:
        ordering = ('date_added',)

    def __str__(self):
        return self.body[0:30]

    
    def get_comments(self):
        return Comment.objects.filter(parent=self).filter(active=True)
