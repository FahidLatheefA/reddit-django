from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()


@register.filter(name='get_type')
def get_type(value):
    return type(value)

@stringfilter
@register.filter(name='add_two_more_tabs')
def add_two_more_tabs(value):
    return value + "&emsp;&emsp;"

@register.simple_tag(name='karmaaddition')
def karmaaddition(value1, value2):
    return int(value1)-int(value2)
