from typing import Text
from subreddit.models import Subreddit
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth import authenticate, login, logout
from posts.forms import CreatePostForm, CreateCommmentForm
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.urls import reverse, reverse_lazy
from posts.models import Comment, TextPost
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views import View
from django.http import HttpResponseRedirect
import datetime


from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)

"""
def content(request):
    posts = TextPost.objects.all().values()
    users =  User.objects.all().values()
    output = []
    print(users)
    for post in posts:
        new_dict = {}
        author_id = post.author_id_id
        author = users[author_id]['username']
        new_dict['author'] = author
        new_dict['title'] = post.title
        new_dict['content'] = post.content
        print(new_dict)
        output.append(new_dict)
    print(output)
    context = {
        'posts': output
    }
    print(context)
    return render(request, 'subreddit/subreddit_detail.html', context)
"""
"""
# Create your views here.
class TextPostListView(ListView):
    model = TextPost
    template_name = 'posts/content.html'
    context_object_name = 'posts'
    ordering = ['-date_posted']
    def get_queryset(self, *args, **kwargs):
        qs = super(TextPostListView, self).get_queryset(*args, **kwargs)
        print(self.kwargs)
        
        qs = qs.order_by("-id")
        return qs
"""
class TextPostDetailView(DetailView):
    model = TextPost
    context_object_name="post"
    def get_queryset(self, *args, **kwargs):
        qs = super(TextPostDetailView, self).get_queryset(*args, **kwargs)
        print(self.kwargs)
        print(qs)
        qs = qs.filter(subreddit_id__name=self.kwargs['slug'])  # Filter post based on particular sub
        print(qs)
        qs = qs.order_by("date_posted")
        print(qs)
        return qs


"""    def get_queryset(self, *args, **kwargs):
        qs = super(TextPostDetailView, self).get_queryset(*args, **kwargs)
        print(self.kwargs)
        print(qs)
        qs = qs.filter(subreddit_id__name=self.kwargs['slug'])  # Filter post based on particular sub
        print(qs)
        qs = qs.order_by("date_posted")
        print(qs)
        return qs
    def get_context_data(self, **kwargs):
        # print(self.request.user)
        context = super().get_context_data(**kwargs)
        absolute_url = self.request.build_absolute_uri()
        sub = ''
        for ele in absolute_url[:-1][::-1]:  # Reversed url except last character
            if ele == "/":
                break # To correctly filter subname from url
            sub+=ele
        sub = sub[::-1]
        context['sub'] = sub
        # print(sub)

        # Filter Specific posts to the sub
        posts_list = TextPost.objects.values().filter(subreddit_id__name=sub)

        users =  User.objects.all().values()
        output = []
        # print(users)
        for post in posts_list:
            new_dict = {}
            # print(post)
            author_id = post['author_id_id']
            # print(author_id)
            try:
                author = users.get(id=author_id)['username']
            except:
                author = 'anonymous'
            new_dict['id'] = post['id']
            new_dict['author'] = author
            new_dict['title'] = post['title']
            new_dict['content'] = post['content']
            new_dict['media'] = post['media_content']
            new_dict['qwert'] = 'qwert'
            print('hi')
            duration = datetime.datetime.utcnow()-post['date_posted']
            print(duration)
            if duration.days>0:
                new_dict['time'] = f'{duration.days} days ago'
            elif duration.seconds>3600:
                hours = int((duration.seconds/60)/24)
                new_dict['time'] = f'{hours} hours ago'
            else:
                minutes = int(duration.seconds/60)
                new_dict['time'] = f'{minutes} minutes ago'
            print(post['media_content'])  # url images/star.jpg

            # print(new_dict)
            output.append(new_dict)
        # print(output)
        context['posts'] = output
        return context

"""

class TextPostCreateView(LoginRequiredMixin, CreateView):
    model = TextPost
    form_class = CreatePostForm
    
    def form_valid(self, form):
        # print(self.request)
        form.instance.author_id_id = self.request.user.id
        # print(self.request.user.id)
        # print(self.kwargs)
        my_sub_name = self.kwargs['slug']
        # print(my_sub_name)
        subs = Subreddit.objects.all().values()
        form.instance.subreddit_id_id = subs.filter(name=my_sub_name)[0]['id']
        form.save()
        form.instance.likes.add(self.request.user)

        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return f"/r/{self.kwargs['slug']}/"


class TextPostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = TextPost
    form_class = CreatePostForm
    

    def form_valid(self, form):
        # print(self.request)
        form.instance.author_id_id = self.request.user.id
        # print(self.request.user.id)
        print(self.kwargs)
        my_sub_name = self.kwargs['slug']
        # print(my_sub_name)
        subs = Subreddit.objects.all().values()
        form.instance.subreddit_id_id = subs.filter(name=my_sub_name)[0]['id']

        return super().form_valid(form)

    def test_func(self):
        post = self.get_object()

        if self.request.user == post.author_id:
            return True
        return False

class CommentCreateView(LoginRequiredMixin, CreateView):
    model = Comment
    template_name = 'posts/add_comment.html'
    form_class = CreateCommmentForm
    subs = Subreddit.objects.all().values()
    

    def form_valid(self, form):
        print(self.request.POST.get('parent_id', None))
        # form.instance.author_id = self.request.user
        form.instance.author_id = self.request.user
        form.instance.post_id = self.kwargs['pk']
        form.instance.subreddit_id = self.subs.get(name=self.kwargs['slug'])['id']
        form.instance.parent_id = self.request.POST.get('parent_id', None)
        form.save()
        form.instance.likes.add(self.request.user)

        return HttpResponseRedirect(self.get_success_url())
    
    def get_success_url(self):
        # print(self.kwargs)
        return reverse(
            'post-details',
            kwargs={
                'slug': self.kwargs['slug'],
                'pk': self.kwargs['pk'],
            }
        )
"""    
    def post(self, request, *args, **kwargs):
        print(self.kwargs)
        post = TextPost.objects.get(pk=self.kwargs['pk'])
        # parent_comment = Comment.objects.get(pk=self.kwargs['id'])
        form = CreateCommmentForm(request.POST)
        subs = Subreddit.objects.all().values()
        print("hhh")
        print(form)

        print("ine")
        new_comment = form.save(commit=False)
        print(new_comment.parent_id)
        print(new_comment)
        new_comment.author_id = request.user
        new_comment.post = post
        new_comment.subreddit_id = subs.get(name=self.kwargs['slug'])['id']
        # new_comment.parent = parent_comment
        print('jogo')
        new_comment.save()
        print('kogo')

        return reverse('post-details', kwargs={
                'slug': self.kwargs['slug'],
                'pk': self.kwargs['pk']
            })
"""
"""class CommentReplyView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        print(self.kwargs)
        post = TextPost.objects.get(pk=self.kwargs['pk'])
        parent_comment = Comment.objects.get(pk=self.kwargs['id'])
        form = CreateCommmentForm(request.POST)

        if form.is_valid():
            new_comment = form.save(commit=False)
            new_comment.author_id = request.user
            new_comment.post = post
            new_comment.parent = parent_comment
            new_comment.save()
         

        return reverse(
                'post-details',
                kwargs={
                    'slug': self.kwargs['slug'],
                    'pk': self.kwargs['pk']
                }
            )
"""

class TextPostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = TextPost
    
    def get_success_url(self, **kwargs):
    # obj = form.instance or self.object
        return reverse("sub-details", kwargs={'slug':self.object.subreddit_id.name})

    def test_func(self):
        post = self.get_object()

        if self.request.user == post.author_id:
            return True
        return False

class AddPostLike(LoginRequiredMixin, View):
    def post(self, request, pk, *args, **kwargs):
        post = TextPost.objects.get(pk=pk)

        is_dislike = False

        for dislike in post.dislikes.all():
            if dislike == request.user:
                is_dislike = True
                break

        if is_dislike:
            post.dislikes.remove(request.user)

        is_like = False

        for like in post.likes.all():
            if like == request.user:
                is_like = True
                break

        if not is_like:
            post.likes.add(request.user)
        
        if is_like:
            post.likes.remove(request.user)

        next = request.POST.get('next', '/')
        return HttpResponseRedirect(next)



class AddPostDislike(LoginRequiredMixin, View):
    def post(self, request, pk, *args, **kwargs):
        post = TextPost.objects.get(pk=pk)

        is_like = False

        for like in post.likes.all():
            if like == request.user:
                is_like = True
                break

        if is_like:
            post.likes.remove(request.user)

        is_dislike = False

        for dislike in post.dislikes.all():
            if dislike == request.user:
                is_dislike = True
                break

        if not is_dislike:
            post.dislikes.add(request.user)
        
        if is_dislike:
            post.dislikes.remove(request.user)

        next = request.POST.get('next', '/')
        return HttpResponseRedirect(next)


class AddCommentLike(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        comment_key = request.POST.get('comment_key', '/')
        print(comment_key)
        comment = Comment.objects.get(pk=comment_key)

        is_dislike = False

        for dislike in comment.dislikes.all():
            if dislike == request.user:
                is_dislike = True
                break

        if is_dislike:
            comment.dislikes.remove(request.user)

        is_like = False

        for like in comment.likes.all():
            if like == request.user:
                is_like = True
                break

        if not is_like:
            comment.likes.add(request.user)

        if is_like:
            comment.likes.remove(request.user)

        next = request.POST.get('next', '/')
        return HttpResponseRedirect(next)

class AddCommentDislike(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):

        comment_key = request.POST.get('comment_key', '/')
        print(comment_key)
        comment = Comment.objects.get(pk=comment_key)

        is_like = False

        for like in comment.likes.all():
            if like == request.user:
                is_like = True
                break

        if is_like:
            comment.likes.remove(request.user)

        is_dislike = False

        for dislike in comment.dislikes.all():
            if dislike == request.user:
                is_dislike = True
                break

        if not is_dislike:
            comment.dislikes.add(request.user)

        if is_dislike:
            comment.dislikes.remove(request.user)

        next = request.POST.get('next', '/')
        return HttpResponseRedirect(next)