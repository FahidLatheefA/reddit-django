"""reddit URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from os import name
from posts.models import TextPost
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from django.contrib.auth import views as auth_views
from users.views import register_view, home_view, logout_view #, login_view
from subreddit.views import create_subreddit
from subreddit.views import SubredditDetailView, SubredditListView
from posts.views import (
    TextPostCreateView,
    TextPostDetailView,
    CommentCreateView,
    TextPostUpdateView,
    TextPostDeleteView,
    AddPostLike,
    AddPostDislike,
    AddCommentDislike,
    AddCommentLike
    )

urlpatterns = [
    path('', home_view, name="home"),
    path('admin/', admin.site.urls),
    path('register/', register_view, name="register"),
    path('login/', auth_views.LoginView.as_view(template_name = "registration/login.html"), name="login"),
    # path('login/', login_view, name="login"),
    # path('logout/', auth_views.LogoutView.as_view(), name="logout"),
    path('logout/', logout_view, name="logout"),
    path('create-sub/', create_subreddit, name="create-sub"),
    path('r/<slug:slug>/', SubredditDetailView.as_view(), name='sub-details'),
    path('r/', SubredditListView.as_view(), name='sub-home'),
    path('r/<slug:slug>/new/',  TextPostCreateView.as_view(), name='post-create'),
    path('r/<slug:slug>/<int:pk>/update/',  TextPostUpdateView.as_view(), name='post-update'),
    # path('r/<slug:slug>/', TextPostListView.as_view(), name = 'individual-sub-home'),
    path('r/<slug:slug>/<int:pk>/', TextPostDetailView.as_view(), name = 'post-details'),
    path('r/<slug:slug>/<int:pk>/delete/',  TextPostDeleteView.as_view(), name='post-delete'),
    path('r/<slug:slug>/<int:pk>/comment',  CommentCreateView.as_view(), name='comment-create'),
    path('r/<slug:slug>/<int:pk>/comment-like', AddCommentLike.as_view(), name = 'comment-like'),
    path('r/<slug:slug>/<int:pk>/comment-dislike', AddCommentDislike.as_view(), name = 'comment-dislike'),
    path('reset-password/', auth_views.PasswordResetView.as_view(template_name="registration/password_reset.html"), name="reset_password"),
    path('reset-password_sent/', auth_views.PasswordResetDoneView.as_view(template_name="registration/password_reset_sent.html"),name="password_reset_done"),
    path('reset-password/<uidb64>/<token>', auth_views.PasswordResetConfirmView.as_view(template_name="registration/password_reset_form.html"), name="password_reset_confirm"),
    path('reset-password-complete/', auth_views.PasswordResetCompleteView.as_view(template_name="registration/password_reset_done.html"), name="password_reset_complete"),
    # path('r/<slug:slug>/<int:pk>/<int:id>', CommentReplyView.as_view(), name='comment-reply'),
    path('r/<slug:slug>/<int:pk>/like', AddPostLike.as_view(), name = 'post-like'),
    path('r/<slug:slug>/<int:pk>/dislike', AddPostDislike.as_view(), name = 'post-dislike'),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
