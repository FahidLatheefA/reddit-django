from django.forms import ModelForm
from subreddit.models import Subreddit
from django import forms


class CreateSubredditForm(ModelForm):
    name = forms.CharField(widget=forms.Textarea(attrs={'class': 'sub-form-control', 'placeholder': 'subreddit-name', 'rows': '1', 'cols':'26'}))
    description = forms.CharField(widget=forms.Textarea(attrs={'class': 'sub-form-control', 'placeholder': 'subreddit-description', 'rows': '12', 'cols':'26'}))
    minimum_karma_to_post = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'sub-form-control', 'placeholder': '0', 'rows': '1'}))
    class Meta:
        model = Subreddit
        fields = ['name', 'description', 'minimum_karma_to_post']

    
    def clean_sub_name(self):
        """
        ensure that sub_name is always lower case.
        """
        return self.cleaned_data['name'].strip().lower()
