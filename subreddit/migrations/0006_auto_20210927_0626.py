# Generated by Django 2.2 on 2021-09-27 06:26

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('subreddit', '0005_subreddit_slug'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subreddit',
            name='name',
            field=models.TextField(max_length=20, unique=True, validators=[django.core.validators.RegexValidator(' ', 'No spaces allowed', code='invalid_tag', inverse_match=True)]),
        ),
    ]
