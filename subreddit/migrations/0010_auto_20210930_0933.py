# Generated by Django 2.2 on 2021-09-30 09:33

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('subreddit', '0009_auto_20210930_0931'),
    ]

    operations = [
        migrations.RenameField(
            model_name='subreddit',
            old_name='minimum_karma_to_post11',
            new_name='minimum_karma_to_post',
        ),
    ]
