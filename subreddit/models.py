from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.core.validators import RegexValidator
from django.urls import reverse
from autoslug import AutoSlugField
from django.utils.text import slugify

# Disallow spaces
no_space_validator = RegexValidator(
      r' ',
      _('No spaces allowed'),
      inverse_match=True,
      code='invalid_tag',
  )

class Subreddit(models.Model):
    name = models.TextField(max_length=20,
     unique=True, null=False,
     validators=[no_space_validator]
     )
    description = models.TextField(max_length=150, null=True, blank=True)
    minimum_karma_to_post = models.IntegerField(default=0)
    creator_id = models.ForeignKey(User, on_delete=models.CASCADE)
    slug = AutoSlugField(populate_from='name')

    # Function to print name with instance
    def __str__(self):
        return self.name
    
    def get_absolute_url(self):
        # return reverse('sub-home')
        return f"/r/{self.slug}/"
