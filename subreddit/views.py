from mimetypes import guess_type

from posts.models import TextPost
from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from .forms import CreateSubredditForm
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.models import User
from subreddit.models import Subreddit
from django.urls import reverse
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from posts.models import TextPost
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)
import datetime

def create_subreddit(request):
    if request.method == 'POST':
        user = request.user
        # print(user)
        form = CreateSubredditForm(request.POST)
        if form.is_valid():
            subreddit = Subreddit()
            # subreddit.creator_id = User.objects.get(username=user).id
            subreddit.creator_id = user
            subreddit.name = form.cleaned_data.get('name')
            subreddit.description = form.cleaned_data.get('description')
            subreddit.minimum_karma_to_post = form.cleaned_data.get('minimum_karma_to_post')
            subreddit.save()
            name = form.cleaned_data.get('name')
            messages.success(request, f'{name} has been created successfully!')
            return redirect('home')
    else:
        form = CreateSubredditForm()
    return render(request, 'subreddit/create_sub.html', {'form': form})

class SubredditListView(ListView):
    model = Subreddit
    template_name = 'subreddit/subreddit_detail.html'
    context_object_name = 'subreddits'
    ordering = ['name']  # Alphabetical order of subreddit names

class SubredditDetailView(DetailView):
    model = Subreddit
    def get_queryset(self, *args, **kwargs):
        qs = super(SubredditDetailView, self).get_queryset(*args, **kwargs)
        
        qs = qs.order_by("id")
        return qs

    def get_context_data(self, **kwargs):
        # print(self.request.user)
        context = super().get_context_data(**kwargs)
        sub = self.kwargs['slug']
        context['sub'] = sub

        # Filter Specific posts to the sub by date
        posts_list = TextPost.objects.all().filter(subreddit_id__name=sub).order_by('-date_posted')

        users =  User.objects.all().values()
        output = []
        # print(users)
        # print("HI")
        # print(posts_list)
        for post in posts_list:
            # print(post)
            new_dict = {}
            # print(post)
            author_id = post.author_id_id
            try:
                author = users.get(id=author_id)['username']
            except:
                author = 'anonymous'
            # print(post)
            new_dict['id'] = post.id
            new_dict['author'] = author
            new_dict['title'] = post.title
            new_dict['content'] = post.content
            new_dict['karma'] = post.likes.all().count()-post.dislikes.all().count()
            print("like:", post.likes.all().count())
            print("dislike:", post.dislikes.all().count())
            print(new_dict['karma'])
           
            # print(post.media_content.url)
            try:
                new_dict['media'] = post.media_content.url
            except:
                new_dict['media'] = ''
            
            # print("**********")
            # print(new_dict['media'])
            # print("*********")
            # Media Type

            # Filter url
            url_start = ""
            for letter in new_dict['media']:
                if letter == '?':
                    break
                else:
                    url_start += letter

            try:
                type_tuple = guess_type(url_start, strict=True)
                # print("Type:", type_tuple[0])
            except:  # Value Error
                type_tuple = [None, None]
                # print("Type:", type_tuple[0])
            if type_tuple[0] is None:
                # print("None")
                new_dict['media_type'] = ""
            elif type_tuple[0].__contains__("image"):
                new_dict['media_type'] = "image"
            elif type_tuple[0].__contains__("video"):
                new_dict['media_type'] = "video"
            else:
                new_dict['media_type'] = "unsupported"
           
            duration = datetime.datetime.utcnow()-post.date_posted
            # print(duration)
            if duration.days>0:
                new_dict['time'] = f'{duration.days} days ago'
            elif duration.seconds>3600:
                hours = int((duration.seconds/60)/24)
                new_dict['time'] = f'{hours} hours ago'
            else:
                minutes = int(duration.seconds/60)
                new_dict['time'] = f'{minutes} minutes ago'
            # print(post['media_content'])  # url images/star.jpg

            # print(new_dict)
            output.append(new_dict)
        # print(output)
        context['posts'] = output
        return context
    def get_absolute_url(self):
        return reverse('sub-home')
