from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=150, help_text='Email')

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']

    def clean_email(self):
        """
        ensure that email is always lower case.
        """
        return self.cleaned_data['email'].lower()
    
    def clean_username(self):
        """
        ensure that username is always lower case.
        """
        allowed_chars = 'abcdefghijklmnopqrstuvwxyz1234567890_'
        data = self.cleaned_data['username']
        if not data.islower():
            raise forms.ValidationError("Usernames should be in lowercase")
        for character in data:
            if character not in allowed_chars:
                raise forms.ValidationError("Only Lowercase English Alphabets, Numbers or _ allowed")
        return data
