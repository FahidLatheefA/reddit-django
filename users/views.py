from mimetypes import guess_type

from django.http import request
from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from users.forms import SignUpForm
from posts.models import TextPost
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.contrib import messages
from subreddit.models import Subreddit
import datetime

def home_view(request):
    subs = []
    for item in Subreddit.objects.values():
        subs.append(item['name'])
    # Latest 20 Posts
    posts_list = TextPost.objects.all().order_by('-date_posted')[0:20]
    users =  User.objects.all().values()
    output = []
    for post in posts_list:
        # print(post)
        new_dict = {}
        # print(post)
        author_id = post.author_id_id
        try:
            author = users.get(id=author_id)['username']
        except:
            author = 'anonymous'
        # print(post)
        new_dict['id'] = post.id
        new_dict['author'] = author
        new_dict['title'] = post.title
        new_dict['content'] = post.content
        new_dict['sub'] = post.subreddit_id
        
        # print(post.media_content.url)
        try:
            new_dict['media'] = post.media_content.url
        except:
            new_dict['media'] = ''
        
        # print("**********")
        # print(new_dict['media'])
        # print("*********")
        # Media Type

        # Filter url
        url_start = ""
        for letter in new_dict['media']:
            if letter == '?':
                break
            else:
                url_start += letter

        try:
            type_tuple = guess_type(url_start, strict=True)
            # print("Type:", type_tuple[0])
        except:  # Value Error
            type_tuple = [None, None]
            # print("Type:", type_tuple[0])
        if type_tuple[0] is None:
            # print("None")
            new_dict['media_type'] = ""
        elif type_tuple[0].__contains__("image"):
            new_dict['media_type'] = "image"
        elif type_tuple[0].__contains__("video"):
            new_dict['media_type'] = "video"
        else:
            new_dict['media_type'] = "unsupported"
        
        duration = datetime.datetime.utcnow()-post.date_posted
        # print(duration)
        if duration.days>0:
            new_dict['time'] = f'{duration.days} days ago'
        elif duration.seconds>3600:
            hours = int((duration.seconds/60)/24)
            new_dict['time'] = f'{hours} hours ago'
        else:
            minutes = int(duration.seconds/60)
            new_dict['time'] = f'{minutes} minutes ago'
        # print(post['media_content'])  # url images/star.jpg

        # print(new_dict)
        output.append(new_dict)
    return render(request, 'home.html', context={'subs':subs, 'posts':output})


def register_view(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            form.save()
            messages.success(request, f'Your account {username} has been created! You are now able to log in')
            return redirect('login')
    else:
        form = SignUpForm()
    return render(request, 'users/register.html', {'form': form})

"""
def login_view(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect('home', context = {'user':user})
        else:
            messages.info(request, 'Try again! username or password is incorrect')

    context = {}
    return render(request, 'users/login.html', context)
"""

def logout_view(request):
    logout(request)
    return redirect('home')















"""
def register_view(request):
    form = SignUpForm(request.POST)
    print("hello")
    if form.is_valid():
        user = form.save()
        print(user)
        # user.refresh_from_db()
        user.save()
        print(user)
        print("hello2")
        print(form.cleaned_data)
        print("break", user.profile)
        user.profile.first_name = form.cleaned_data.get('first_name')
        print("hello2a")
        user.profile.last_name = form.cleaned_data.get('last_name')
        print("hello2b")
        user.profile.email = form.cleaned_data.get('email')
        print("hello3")
        user.save()
        print("hello4")
        return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'register.html', {'form': form})
"""

"""

        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password1')
        print("hello5")
        print(form.cleaned_data)
        user = authenticate(username=username, password=password)
        print("user-details\n", user)
        login(request, user)
        print("checking login")
"""